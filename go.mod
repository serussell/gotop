module github.com/xxxserxxx/gotop

require (
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/distatus/battery v0.9.0
	github.com/docopt/docopt.go v0.0.0-20180111231733-ee0de3bc6815
	github.com/gizak/termui/v3 v3.0.0
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/mattn/go-runewidth v0.0.4
	github.com/shirou/gopsutil v2.18.11+incompatible
	github.com/shirou/w32 v0.0.0-20160930032740-bb4de0191aa4 // indirect
	github.com/stretchr/testify v1.4.0
	golang.org/x/sys v0.0.0-20200212091648-12a6c2dcc1e4 // indirect
	howett.net/plist v0.0.0-20181124034731-591f970eefbb // indirect
)

go 1.13
